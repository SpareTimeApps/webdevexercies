var p1b = document.getElementById("p1");
var p1Score = 0;
var from = document.querySelector("#from");
var to = document.querySelector("#to");
var numInput = document.querySelector("#numInput");
var winningScoreDisplay = document.querySelector("p>span");

var gameOver = false;
winningScore = 5;
p1b.addEventListener("click", function() {
    if (!gameOver) {
        p1Score++;
        if (p1Score === winningScore) {
            from.classList.add("winner");
            gameOver = true;
        }
        from.textContent = p1Score;
    }
});
var p2b = document.getElementById("p2");
var p2Score = 0;
p2b.addEventListener("click", function() {
    if (!gameOver) {
        p2Score++;
        console.log(winningScore);
        if (p2Score === winningScore) {

            to.classList.add("winner");
            gameOver = true;
        }
        to.textContent = p2Score;
    }
});

function resetf() {
    gameOver = false;
    p1Score = 0;
    p2Score = 0;
    from.textContent = 0;
    to.textContent = 0;
    from.classList.remove("winner");
    to.classList.remove("winner");
}

var reset = document.getElementById("p3");
reset.addEventListener("click", function() {
    resetf();
});

numInput.addEventListener("change", function() {

    winningScoreDisplay.textContent = Number(this.value);
    winningScore = Number(this.value);
    console.log(winningScore);
    resetf();
});
