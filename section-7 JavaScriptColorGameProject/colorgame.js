var num = 3;
var colorDisplay = document.getElementById("colorDisplay");
var squares = document.querySelectorAll(".square");
var clickMessage = document.querySelector("#clickMessage");
var h1 = document.querySelector("h1");
var stripeColorDisplay = document.querySelector("#stripe");
var resetButton = document.querySelector("#reset");
var easy = document.querySelector("#easy");
var hard = document.querySelector("#hard");

var colors = mode(num);
var gameColor = generateGameColor();
startGame();

easy.addEventListener("click", function() {
    num = 3;
    mode(num);
    gameColor = generateGameColor();
    startGame();
});
hard.addEventListener("click", function() {
    num = 6;
    mode(num);
    gameColor = generateGameColor();
    startGame();

});


function startGame() {
    console.log(colors);
    console.log(gameColor);
    for (var i = 0; i < squares.length; i++) {
        squares[i].style.background = colors[i];
        squares[i].addEventListener("click", function() {
            var clickedColor = this.style.background;
            if (clickedColor == gameColor) {
                clickMessage.textContent = "Correct!!";
                changeColors(clickedColor);
                h1.style.background = gameColor;
                resetButton.textContent = "Play Again?";
            } else {
                this.style.background = "#232323";
                clickMessage.textContent = "try again!!";
            }
        });
    }
}

function changeColors(color) {
    //loop thro all the squraqes to change their colors.
    for (var i = 0; i < squares.length; i++) {
        squares[i].style.background = color;
    }
}

function generateGameColor() {
    var rand = Math.floor(Math.random() * (colors.length - 1) + 1);
    return colors[rand];
}

function generateRandomColors(num) {
    var arr = [];
    for (var i = 0; i < num; i++) {
        arr.push(randomColor());
    }
    return arr;
}

function randomColor() {
    var r = Math.floor(Math.random() * 256);
    // pick a red from 0-255
    var g = Math.floor(Math.random() * 256);
    // pick a green from 0-255
    var b = Math.floor(Math.random() * 256);
    // pick a blue from 0-255
    var ret = "rgb(" + r + ", " + g + ", " + b + ")";
    return ret;
}
resetButton.addEventListener("click", function() {
    hard.classList.remove("selected");
    easy.classList.remove("selected");
    resetButton.classList.add("selected");
    colors = generateRandomColors(num);
    gameColor = generateGameColor();
    colorDisplay.textContent = gameColor;
    for (var i = 0; i < squares.length; i++) {
        squares[i].style.background = colors[i];
    }
    h1.style.background = "steelblue";
    clickMessage.textContent = "";
    resetButton.textContent = "New Colors";
    num = 3;
    startGame();
});

function mode(num) {
    colors = generateRandomColors(num);
    if (num === 6) {
        hard.classList.add("selected");
        easy.classList.remove("selected");
        for (var i = 0; i < squares.length; i++) {
            squares[i].style.background = colors[i];
            squares[i].style.display = "block";
        }
    } else {
        hard.classList.remove("selected");
        easy.classList.add("selected");

        for (var i = 0; i < squares.length; i++) {
            if (colors[i]) {
                squares[i].style.background = colors[i];
            } else {
                squares[i].style.display = "none";
            }
        }
    }
    return colors;
}
